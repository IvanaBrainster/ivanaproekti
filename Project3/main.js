$(document).ready(function() {
    //Questionmark on click
    $(".questionmark, .btn").on('click', function() {
        $(".wrapbox").slideToggle();
        $(".uparrow").slideToggle();
    });
    //menu click 
    $(".menu-toggle").on('click', function() {
        $(".overlay-content").slideToggle();
    });
    //filter click
    $(".filterwrap .filter").on('click', function() {
            $(this).toggleClass('filter2');
    });
    $(".filterwrap .filter.all").on('click', function(){
        $('.energy, .inovacii, .leader, .action, .tim').removeClass('filter2');
    });

    $(".filterwrap .filter.energy").on('click', function(){
        $('.all').removeClass('filter2');
    });
    $(".filterwrap .filter.inovacii").on('click', function(){
        $('.all').removeClass('filter2');
    });
    $(".filterwrap .filter.leader").on('click', function(){
        $('.all').removeClass('filter2');
    });
    $(".filterwrap .filter.action").on('click', function(){
        $('.all').removeClass('filter2');
    });
    $(".filterwrap .filter.tim").on('click', function(){
        $('.all').removeClass('filter2');
    });



    //show scrollToTop button
    $(window).scroll(function() {
        if ($(this).scrollTop() > 700) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });

    //smooth scroling to top
    $('.scrollToTop').click(function() {
        $('html,body').animate({ scrollTop: 0 }, 1000)
    });



});