
$(function() {

    setupScore();

    colorize();

    var maxPointsHard = 50;
    var maxPointsEasy = 20;
    var count = 0;

    function setupScore() {
        var score = localStorage.getItem("score");
        if(score == null) {
            score = 0;
        }

        $("#score").html(score);
    }

    function getRandomColor() {
        var r = Math.ceil(Math.random() * 255);
        var g = Math.ceil(Math.random() * 255);
        var b = Math.ceil(Math.random() * 255);

        return 'rgb(' + r + ", " + g + ", " + b + ")";
    }

    function colorize() {
        var divs = $(".square:visible").length;
        var colors = [];

        for(var i = 0; i < divs; i++) {
            var randomColor = getRandomColor();
            colors.push( randomColor );
            $(".square").eq(i).css("background-color", randomColor);
            
        }

        $("#colorDisplay").text( colors[Math.floor(Math.random()*colors.length)] );
        $("h1").css("background-color", 'steelblue');
        $("#reset").text("New colors");
        $("#message").text("");
    }


    $("#reset").on("click", function() {
        redraw();
        colorize();    
        
    });

    $(".mode").on("click", function() {
        $(".mode").removeClass("selected");
        $(this).addClass("selected");

        redraw();

        colorize();
    });
    
    function redraw() {
        count = 0;
        var elem = $(".mode.selected");
        if($(elem).text() == 'Easy') {
            var length = $(".square").length;
            var toShow = Math.floor(length / 2);
            $(".square").hide();
            for(var i = 0; i < toShow; i++) {
                $(".square").eq(i).show();
            }
        } else {
            $(".square").show();
        }
    }

    $(document).on('click', '.square', function() {
        var elemColor = $(this).css("background-color");
        var colorToCompare = $("#colorDisplay").text();
        
        if(elemColor == colorToCompare) {
            $("#message").text("You won");
            $("h1").css('background-color', elemColor);
            $(".square:visible").css('background-color', elemColor);
            $("#reset").text("Play again...");

            calculateScore();         
        } else {
            $(this).fadeOut();
            $("#message").text("Try again");
        }        

        count++;
    });

    function calculateScore() {
        var mode = $(".mode.selected").data("mode");

        if(mode == 'easy') {
             score = maxPointsEasy - count * 10;
        } else if(mode == 'hard') {
            score = maxPointsHard - count * 10;
        }

        var prevScore = localStorage.getItem("score");
        if(prevScore == null) {
            prevScore = 0;
        }
        
        var newScore = parseInt(prevScore) + score;

        localStorage.setItem("score", newScore);

        setupScore();
    }
});